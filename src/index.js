import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {

  renderSquare(i) {
    return (<Square value={this.props.squares[i]}
                    onClick={() => this.props.onClick(i)} />
    );
  }

  render() {
    return (
      <div>
       
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      status: 'Next player: ',
      nextPlayer: 'X',
    };
  }

  squareClicked(i) {
    let arr = [...this.state.history];
    var current = arr[arr.length-1]

    //already clicked, or already a winner -- ignore the click
    if (calculateWinner(current.squares) || current.squares[i]) {
      return;
    }

    current.squares[i] = this.state.nextPlayer;

    let winner = calculateWinner(current.squares)
    if (winner) {
      this.setState({ history: arr.concat(current), status: "The winner is: " })
    }
    else {
      this.setState({ history: arr.concat(current), nextPlayer: this.state.nextPlayer === 'X' ? 'O' : 'X' })
    }
  
    
  }
  render() {
    const history = this.state.history;
    const current = history[history.length - 1];
    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares= {current.squares}
            onClick={(i) => this.squareClicked(i)} />
        </div>
        <div className="game-info">
        <div className="status">{this.state.status} {this.state.nextPlayer}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

// ========================================

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
